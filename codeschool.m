#import "AppDelegate.h"

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  CGRect viewRect = [[UIScreen mainScreen] bounds];
  self.window = [[UIWindow alloc] initWithFrame:viewRect];
  UIViewController *colorTouchVc  = [[UIViewController alloc] init];
  UIView *colorView = [[UIView alloc] initWithFrame:viewRect];
  [colorView setBackgroundColor:[UIColor greenColor]];
  colorTouchVc.view = colorView;

  self.window.rootViewController = colorTouchVc;
  [self.window makeKeyAndVisible];
  NSLog(@"hello mvj3");

  return YES;
}
@end

